from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(city, state):

    headers = {"Authorization": PEXELS_API_KEY}
    url ="https://api.pexels.com/v1/search"
    params = {
        "query": city +" " + state,
        "per_page": 1
    }
    response= requests.get(url, params=params, headers=headers)
    content= json.loads(response.content)
    image= {"image": content["photos"][0]["src"]["original"]}
    return image


def get_weather(city, state):

    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": city +","+ state +","+ "US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }

    response = requests.get(url, params=params, headers=headers)

    content= json.loads(response.content)

    # gps= {"gps": str(content[0]["lat"]) + "," +str(content[0]["lon"])}


    url_weather = "https://api.openweathermap.org/data/2.5/weather"
    params_weather = {
        "lat":content[0]["lat"],
        "lon":content[0]["lon"],
        "appid":OPEN_WEATHER_API_KEY,
    }
    response_weather= requests.get(url_weather, params=params_weather, headers=headers)
    content_weather= json.loads(response_weather.content)
    weather = {
        "temperature": content_weather["main"]["temp"],
        "description": content_weather["weather"][0]["description"],
    }
    return weather
